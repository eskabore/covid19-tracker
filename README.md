# On the way to recovery

Covid App to display the progress made so far.

## Covid tracker

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

## Learn More


### Code Splitting



### Analyzing the Bundle Size



### Making a Progressive Web App



### Advanced Configuration



### Deployment



### `yarn build` fails to minify

